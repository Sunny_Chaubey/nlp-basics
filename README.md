This project is simple demonstration of [stanford core nlp](http://nlp.stanford.edu/). This project is intended for learning NLP and give some idea of using the nlp library in java.

## Clone

```git clone git@bitbucket.org:vchaturvedi12/nlp.git```

## Build and Run NLP Project
- Go to the project folder
- Run ```gradlew clean build run```
- Follow the instructions on the console

## Import to Eclipse
- Run ```gradlew eclipse```
- Import to eclipse ``` File->Import->General->Existing Projects into the workspace```
- Select the project folder in popo up file browser and finish
- Project will be created in eclipse

## Run in Eclipse
- Right click on java file ```com.nlp.NLPMain.java->Run As->Java Application```
- Follow the instructions on the console
- Provide input and press enter key
