package com.nlp;

import com.google.common.io.Files;
import edu.stanford.nlp.ling.CoreAnnotations.NamedEntityTagAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.PartOfSpeechAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.SentencesAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TextAnnotation;
import edu.stanford.nlp.ling.CoreAnnotations.TokensAnnotation;
import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.pipeline.Annotation;
import edu.stanford.nlp.pipeline.StanfordCoreNLP;
import edu.stanford.nlp.semgraph.SemanticGraph;
import edu.stanford.nlp.semgraph.SemanticGraphCoreAnnotations.CollapsedCCProcessedDependenciesAnnotation;
import edu.stanford.nlp.trees.Tree;
import edu.stanford.nlp.trees.TreeCoreAnnotations.TreeAnnotation;
import edu.stanford.nlp.util.CoreMap;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class NLPMain
{
    public static void main(String[] args) throws IOException
    {
        System.out.println("NLP started...");
        Properties props = new Properties();
        props.put("annotators", "tokenize, ssplit, pos, lemma, ner, parse, dcoref");
        StanfordCoreNLP pipeline = new StanfordCoreNLP(props);

        Scanner reader = new Scanner(System.in); // Reading from System.in
        displayMessage();
        System.out.println("Waiting for input ...");
        while (reader.hasNext())
        {
            String line = reader.nextLine();
            switch (line.trim().toLowerCase())
            {
                case "h":
                    displayMessage();
                    break;
                case "q":
                    System.out.println("Ta Ta ... Bye Bye");
                    reader.close();
                    System.exit(0);
                case "f":
                    System.out.println("File Path: ");
                    String path = reader.nextLine();
                    File inputFile = new File(path);
                    System.out.println("Processing ...");
                    String text = Files.toString(inputFile, Charset.forName("UTF-8"));
                    process(pipeline, text);
                    break;
                case "t":
                    System.out.println("Enter Your Text");
                    line = reader.nextLine();
                default:
                    process(pipeline, line);
                    break;
            }
            System.out.println("Waiting for input ...");
        }
        
    }

    private static void displayMessage()
    {
        System.out.println("********************************************");
        System.out.println("\"H + Return\" for help");
        System.out.println("\"F + Return\" to process file");
        System.out.println("\"T + Return\" to process text");
        System.out.println("\"Q + Return\" to exit");
        System.out.println("Type text and press return to process");
        System.out.println("*********************************************");
    }

    public static void process(StanfordCoreNLP pipeline, String text)
    {
        System.out.println("-------------------------------------------");
        // create an empty Annotation just with the given text
        Annotation document = new Annotation(text);
        // run all Annotators on this text
        pipeline.annotate(document);
        // these are all the sentences in this document
        // a CoreMap is essentially a Map that uses class objects as keys and has values with custom
        // types
        List<CoreMap> sentences = document.get(SentencesAnnotation.class);
        for (CoreMap sentence : sentences)
        {
            // traversing the words in the current sentence
            // a CoreLabel is a CoreMap with additional token-specific methods
            for (CoreLabel token : sentence.get(TokensAnnotation.class)) {
                // this is the text of the token
                String word = token.get(TextAnnotation.class);
                // this is the POS tag of the token
                String pos = token.get(PartOfSpeechAnnotation.class);
                // this is the NER label of the token
                String ne = token.get(NamedEntityTagAnnotation.class);

                System.out.println("WORD: " + word + " POS: " + pos + " NE:" + ne);
            }
            // this is the parse tree of the current sentence
            Tree tree = sentence.get(TreeAnnotation.class);
            System.out.println("PARSE TREE:\n" + tree);
            // this is the Stanford dependency graph of the current sentence
            SemanticGraph dependencies = sentence
                .get(CollapsedCCProcessedDependenciesAnnotation.class);
            System.out.println("DEPENDENCY GRAPH:\n" + dependencies);
        }

        // This is the coreference link graph
        // Each chain stores a set of mentions that link to each other,
        // along with a method for getting the most representative mention
        // Both sentence and token offsets start at 1!
        /*
         * Map<Integer, CorefChain> graph = document.get(CorefChainAnnotation.class);
         * System.out.println("COREFERENCE GRAPH:\n" + graph.toString());
         */
        System.out.println("-------------------------------------------");
    }

}
